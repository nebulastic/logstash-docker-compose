# logstash-docker-compose

Logstash example with docker compose that contains some simple input, filter and output plugins. Run `docker compose up` to get started.
